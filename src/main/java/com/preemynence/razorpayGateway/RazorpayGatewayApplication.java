package com.preemynence.razorpayGateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RazorpayGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(RazorpayGatewayApplication.class, args);
	}

}
