package com.preemynence.razorpayGateway.model;

import lombok.Data;

@Data
public class OrderRequest {
	String customerName;
	String email;
	String phoneNumber;
	String amount;
}
